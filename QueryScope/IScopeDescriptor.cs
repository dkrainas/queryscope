﻿namespace QueryScope
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IScopeDescriptor
    {
        string Name
        {
            get;
            set;
        }

        Type Determinator
        {
            get;
            set;
        }

        Type Exceptor
        {
            get;
            set;
        }

        string[] Using
        {
            get;
            set;
        }

        bool AllowBlank
        {
            get;
            set;
        }

        bool InvertLogic
        {
            get;
            set;
        }

        bool UseWildcardMatch
        {
            get;
            set;
        }
    }
}
