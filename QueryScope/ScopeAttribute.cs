﻿namespace QueryScope
{
    using System;
    using System.Linq.Expressions;

    [AttributeUsage(AttributeTargets.Property)]
    public class ScopeAttribute : Attribute
    {
        public ScopeAttribute()
        {
            this.Filter = typeof(DefaultScopeFilter<>);
        }

        public ScopeAttribute(string identifier)
            : this()
        {
            this.Identifier = identifier;
        }

        public string Identifier
        {
            get;
            set;
        }

        public Type Filter
        {
            get;
            set;
        }
    }
}
