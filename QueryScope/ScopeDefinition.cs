﻿namespace QueryScope
{
    using System;
    using System.Reflection;

    public sealed class ScopeDefinition : IScopeDefinition
    {
        internal ScopeDefinition(ScopeAttribute attr, PropertyInfo property)
        {
            this.Identifier = attr.Identifier;
            this.Filter = attr.Filter;
            this.Property = property;
        }

        public string Identifier
        {
            get;
            set;
        }

        public Type Filter
        {
            get;
            set;
        }

        public PropertyInfo Property
        {
            get;
            set;
        }

        public IScopeFilter<T> MakeFilter<T>()
        {
            var filterType = this.Filter;
            if (this.Filter.IsGenericTypeDefinition)
            {
                filterType = this.Filter.MakeGenericType(new[] { typeof(T) });
            }

            return Activator.CreateInstance(filterType) as IScopeFilter<T>;
        }
    }
}
