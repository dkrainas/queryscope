﻿namespace QueryScope
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IScopeDeterminator
    {
        bool IsApplicable(dynamic value);
    }
}
