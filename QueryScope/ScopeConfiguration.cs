﻿namespace QueryScope
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Web;

    public class ScopeConfiguration
    {
        public static readonly string ScopeConfigurationCollectionKey = "queryscope.config";

        private static ScopeConfiguration _staticCurrent;

        public static ScopeConfiguration Current
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    if (ScopeConfiguration._staticCurrent == null)
                    {
                        ScopeConfiguration._staticCurrent = new ScopeConfiguration();
                    }

                    return ScopeConfiguration._staticCurrent;
                }
                else
                {
                    return ScopeConfiguration.GetRequestStatic(HttpContext.Current);
                }
            }
        }

        public static ScopeConfiguration GetRequestStatic(HttpContext http)
        {
            return ScopeConfiguration.GetRequestStatic(new HttpContextWrapper(http));
        }

        public static ScopeConfiguration GetRequestStatic(HttpContextBase http)
        {
            if(http == null)
            {
                throw new ArgumentNullException("http");
            }

            var config = http.Items[ScopeConfiguration.ScopeConfigurationCollectionKey] as ScopeConfiguration;
            if (config == null)
            {
                config = new ScopeConfiguration();
                http.Items.Add(ScopeConfiguration.ScopeConfigurationCollectionKey, config);
            }

            return config;
        }

        public ScopeConfiguration()
        {
            this.Scopes = new List<IScopeDescriptor>();
            this.Params = new Dictionary<string, object>();
        }

        public List<IScopeDescriptor> Scopes
        {
            get;
            private set;
        }

        public Dictionary<string, object> Params
        {
            get;
            private set;
        }

        public void HasScope(IScopeDescriptor descriptor)
        {
            if (!Scopes.Contains(descriptor))
            {
                this.Scopes.Add(descriptor);
            }
        }

        public bool IsValidScope(string scopeName)
        {
            return this.Scopes.Any(s => s.Name.Equals(scopeName));
        }

        public void AddParamIfNotExists(string name, object value)
        {
            if (!this.Params.ContainsKey(name))
            {
                this.Params.Add(name, value);
            }
        }
    }
}
