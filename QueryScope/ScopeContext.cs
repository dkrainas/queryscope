﻿namespace QueryScope
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    public sealed class ScopeContext
    {
        public ScopeContext(IDictionary<string, object> parameters, IScopeDescriptor scope, PropertyInfo property)
        {
            this.Params = new Dictionary<string, object>(parameters);
            this.InvertLogic = scope.InvertLogic;
            this.UseWildcardMatch = scope.UseWildcardMatch;
            this.Scope = scope.Name;
            this.Property = property;
        }

        public IDictionary<string, object> Params
        {
            get;
            private set;
        }

        public PropertyInfo Property
        {
            get;
            private set;
        }

        public string Scope
        {
            get;
            private set;
        }

        public bool InvertLogic
        {
            get;
            private set;
        }

        public bool UseWildcardMatch
        {
            get;
            private set;
        }
    }
}
