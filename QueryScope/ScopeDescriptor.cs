﻿namespace QueryScope
{
    using System;

    public class ScopeDescriptor : IScopeDescriptor
    {
        public ScopeDescriptor(string scopeIdentifier)
        {
            this.Name = scopeIdentifier;
            this.Determinator = null;
            this.Exceptor = null;
            this.AllowBlank = false;
            this.InvertLogic = false;
            this.UseWildcardMatch = false;
            this.Using = null;
        }

        public string Name
        {
            get;
            set;
        }

        public Type Determinator
        {
            get;
            set;
        }

        public Type Exceptor
        {
            get;
            set;
        }

        public bool AllowBlank
        {
            get;
            set;
        }

        public bool InvertLogic
        {
            get;
            set;
        }

        public bool UseWildcardMatch
        {
            get;
            set;
        }

        public string[] Using
        {
            get;
            set;
        }
    }
}
