﻿namespace QueryScope
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IScopeExceptor
    {
        bool IsException(dynamic value);
    }
}
