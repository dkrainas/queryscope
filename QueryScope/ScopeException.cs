﻿namespace QueryScope
{
    using System;

    public sealed class ScopeException : Exception
    {
        public ScopeException(string scopeName, Exception innerException)
            : base("An error occurred while filtering the sequence. See inner exception for more details", innerException)
        {
            this.ScopeName = scopeName;
            this.Context = null;
        }

        public ScopeException(ScopeContext context, Exception innerException)
            : base("An error occurred while filtering the sequence. See inner exception for more details", innerException)
        {
            if (context != null)
            {
                this.ScopeName = context.Scope;
            }
            else
            {
                this.ScopeName = "{unknown}";
            }

            this.Context = context;
        }

        public string ScopeName
        {
            get;
            private set;
        }

        public ScopeContext Context
        {
            get;
            private set;
        }
    }
}
