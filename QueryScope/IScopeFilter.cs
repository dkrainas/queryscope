﻿namespace QueryScope
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;

    public interface IScopeFilter<T>
    {
        Expression<Func<T, bool>> Where(ScopeContext context);
    }
}
