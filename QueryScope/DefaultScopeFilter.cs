﻿namespace QueryScope
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;

    public class DefaultScopeFilter<T> : IScopeFilter<T>
    {
        public Expression<Func<T, bool>> Where(ScopeContext context)
        {
            var v = context.Params.FirstOrDefault(kv => kv.Key.Equals(context.Scope, StringComparison.InvariantCultureIgnoreCase)).Value;
            if (v == null)
            {
                return null;
            }

            Expression body = null, member = null, value = null;
            var tParam = Expression.Parameter(typeof(T), "p");
            member = Expression.MakeMemberAccess(tParam, context.Property);
            value = Expression.Constant(v);
            if (typeof(bool?).IsAssignableFrom(v.GetType()))
            {
                var b = v as bool?;
                if (b == null)
                {
                    return null;
                }

                if (!context.InvertLogic && b == true)
                {
                    body = member;
                }
                else
                {
                    body = Expression.Not(member);
                }
            }

            if (context.UseWildcardMatch)
            {
                var contains = typeof(string).GetMethod("Contains", new[] { typeof(string) });
                if (contains != null)
                {
                    body = Expression.Call(member, contains, value);
                }
            }
            else
            {
                if (context.InvertLogic)
                {
                    body = Expression.NotEqual(member, value);
                }
                else
                {
                    body = Expression.Equal(member, value);
                }
            }

            return Expression.Lambda<Func<T, bool>>(body, tParam);
        }
    }
}
