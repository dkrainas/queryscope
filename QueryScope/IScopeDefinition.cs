﻿namespace QueryScope
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    public interface IScopeDefinition
    {
        string Identifier
        {
            get;
            set;
        }

        Type Filter
        {
            get;
            set;
        }

        PropertyInfo Property
        {
            get;
            set;
        }

        IScopeFilter<T> MakeFilter<T>();
    }
}
