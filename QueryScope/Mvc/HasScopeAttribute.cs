﻿namespace QueryScope.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class HasScopeAttribute : ActionFilterAttribute, IScopeDescriptor
    {
        public HasScopeAttribute()
        {
            this.Name = null;
            this.AllowBlank = false;
            this.Exceptor = null;
            this.Determinator = null;
            this.InvertLogic = false;
            this.UseWildcardMatch = false;
            this.Using = null;
        }

        public HasScopeAttribute(string identifier)
            : this()
        {
            this.Name = identifier;
        }

        public string Name
        {
            get;
            set;
        }

        public Type Determinator
        {
            get;
            set;
        }

        public Type Exceptor
        {
            get;
            set;
        }

        public bool AllowBlank
        {
            get;
            set;
        }

        public bool InvertLogic
        {
            get;
            set;
        }

        public bool UseWildcardMatch
        {
            get;
            set;
        }

        public string[] Using
        {
            get;
            set;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Func<KeyValuePair<string, object>, bool> paramFilter = p => p.Key.Equals(this.Name, StringComparison.InvariantCultureIgnoreCase);
            if (filterContext.ActionParameters.Any(paramFilter))
            {
                var param = filterContext.ActionParameters.FirstOrDefault(paramFilter);
                var config = ScopeConfiguration.Current;
                config.AddParamIfNotExists(param.Key, param.Value);
                config.Scopes.Add(this);
            }
        }
    }
}
