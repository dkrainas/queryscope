﻿namespace QueryScope
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;

    public static class QueryableExtensions
    {
        private static readonly Dictionary<Type, List<IScopeDefinition>> _scopeIndicies = new Dictionary<Type, List<IScopeDefinition>>();

        private static List<IScopeDefinition> Index<T>()
        {
            var t = typeof(T);
            if (QueryableExtensions._scopeIndicies.ContainsKey(t))
            {
                return QueryableExtensions._scopeIndicies[t];
            }

            var scopes = new List<IScopeDefinition>();
            foreach (var p in t.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.GetProperty))
            {
                var attrs = p.GetCustomAttributes(typeof(ScopeAttribute), true).Cast<ScopeAttribute>().ToList();
                if (attrs.Count > 0)
                {
                    foreach (var a in attrs)
                    {
                        scopes.Add(new ScopeDefinition(a, p));
                    }
                }
            }

            QueryableExtensions._scopeIndicies.Add(t, scopes);
            return scopes;
        }

        public static IQueryable<T> Scope<T>(this IQueryable<T> source)
        {
            var config = ScopeConfiguration.Current;
            return QueryableExtensions.Scope<T>(source, config);
        }

        public static IQueryable<T> Scope<T>(this IQueryable<T> source, ScopeConfiguration config)
        {
            var scopes = QueryableExtensions.Index<T>();
            var definedScopes = scopes.Where(s => config.IsValidScope(s.Identifier));
            foreach(var descriptor in config.Scopes)
            {
                foreach(var s in scopes.Where(s => s.Identifier.Equals(descriptor.Name)))
                {
                    var context = new ScopeContext(config.Params, descriptor, s.Property);
                    try
                    {
                        var filter = s.MakeFilter<T>();
                        if (filter != null)
                        {
                            var predicate = filter.Where(context);
                            if (predicate != null)
                            {
                                source = source.Where(predicate);
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        throw new ScopeException(context, e);
                    }
                }
            }

            return source;
        }
    }
}
