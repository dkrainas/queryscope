# QueryScope

QueryScope was inpired by the gem `has_scope` for Rails. QueryScope will filter any `IQueryable<T>` based on the current request variables and any custom named scopes applied. 

# Installation

QueryScope available through nuget! To install from the package manager:

    PM> install-package queryscope

For more information related to QueryScope and the package manager, please see [http://nuget.org/packages/QueryScope][2]

# Getting Started

## Declarative Usage

First we will need our own scopes.

    :::csharp
        public class PersonScopes
        {
            public const string Marriage = "Marriage";

            public const string LastName = "LastName";
        }

The purpose of our scopes enum is nothing more than an identifier. Now let us add these scopes to our model and specify the `IScopeFilter<T>` types we want to associate with each:

    :::csharp
        public class Person
        {
            [Scope(PersonScopes.Marriage)]
            public bool IsMarried
            {
                get;
                set;
            }

            public string FirstName
            {
                get;
                set;
            }

            [Scope(PersonScopes.LastName)]
            public string LastName
            {
                get;
                set;
            }
        }

Now in our action we specify what scope(s) we are under using the `HasScopeAttribute` and then call `Scope` on our collection and return it with our view:

    :::csharp
        public class PeopleController : Controller
        {
            private IRepository<Person> repository = new Repository<Person>();

            [HasScope(PersonScopes.Marriage)]
            public ActionResult Index()
            {
                return View(this.repository.Scope().ToList());
            }
        }


## Block Usage

Alternatively, you can specify the scope a collection is under by calling the `HasScope` method on the `ScopeConfiguration` class. Because the library isn't directly connected to any one framework, we must also pass in the parameter the scope will use:

    :::csharp
        var config = ScopeConfiguration.Current;
        config.AddParamIfNotExists("foo", "bar");
        config.HasScope(new ScopeDescriptor(PersonScopes.LastName));

# Bugs and Feedback

If you see a bug or have a suggestion, feel free to create an issue [here][3].

# License

MIT License. Copyright 2013 Daniel Krainas [http://www.danielkrainas.com][1]

[1]: http://www.danielkrainas.com
[2]: http://nuget.org/packages/QueryScope
[3]: https://bitbucket.org/dkrainas/queryscope/issues
