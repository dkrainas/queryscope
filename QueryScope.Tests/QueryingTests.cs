﻿namespace QueryScope.Tests
{
    using System;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using QueryScope.Tests.TestBench;
    using System.Linq;

    [TestClass]
    public class QueryingTests
    {
        private ScopeConfiguration config;

        private IQueryable<Person> people;

        [TestInitialize]
        public void Initialize()
        {
            this.config = new ScopeConfiguration();
            var l = new List<Person>(5);
            l.Add(new Person("daniel", "krainas", false));
            l.Add(new Person("john", "doe", true));
            l.Add(new Person("kathy", "krainas", true));
            this.people = l.AsQueryable();
        }

        [TestMethod]
        public void simple_query_should_return_two()
        {
            config.AddParamIfNotExists("lastname", "krainas");
            config.HasScope(new ScopeDescriptor(PersonScopes.LastName));
            var results = this.people.Scope(config).ToList();
            Assert.AreEqual(2, results.Count);
        }

        [TestMethod]
        public void inverse_query_should_return_one()
        {
            config.AddParamIfNotExists("lastname", "krainas");
            config.HasScope(new ScopeDescriptor(PersonScopes.LastName) { InvertLogic = true });
            var results = this.people.Scope(config).ToList();
            Assert.AreEqual(1, results.Count);
        }

        [TestMethod]
        public void wildcard_query_should_return_two()
        {
            config.AddParamIfNotExists("lastname", "krain");
            config.HasScope(new ScopeDescriptor(PersonScopes.LastName) { UseWildcardMatch = true });
            var results = this.people.Scope(config).ToList();
            Assert.AreEqual(2, results.Count);
        }

        [TestMethod]
        public void no_scope_query_should_return_three()
        {
            var results = this.people.Scope(config).ToList();
            Assert.AreEqual(3, results.Count);
        }

        [TestMethod]
        public void boolean_scope_query_should_return_two()
        {
            config.AddParamIfNotExists("marriage", true);
            config.HasScope(new ScopeDescriptor(PersonScopes.Marriage));
            var results = this.people.Scope(config).ToList();
            Assert.AreEqual(2, results.Count);
        }

        [TestMethod]
        public void no_matching_params_query_should_return_three()
        {
            config.AddParamIfNotExists("foo", "bar");
            config.HasScope(new ScopeDescriptor(PersonScopes.LastName));
            var results = this.people.Scope(config).ToList();
            Assert.AreEqual(3, results.Count);
        }
    }
}
