﻿namespace QueryScope.Tests.TestBench
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Person
    {
        public Person(string firstName, string lastName, bool isMarried)
        {
            this.IsMarried = isMarried;
            this.FirstName = firstName;
            this.LastName = lastName;
        }

        [Scope(PersonScopes.Marriage)]
        public bool IsMarried
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        [Scope(PersonScopes.LastName)]
        public string LastName
        {
            get;
            set;
        }
    }
}
