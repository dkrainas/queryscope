﻿namespace QueryScope.Demo.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class Graduation
    {
        [Scope(GraduationScopes.Featured)]
        public bool IsFeatured
        {
            get;
            set;
        }

        [Scope(GraduationScopes.ByDegree)]
        public string Degree
        {
            get;
            set;
        }
    }
}